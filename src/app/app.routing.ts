import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from '@universis/common';
import { FullLayoutComponent } from './layouts/full-layout.component';
import {LangComponent} from './students-shared/lang-component';

export const routes: Routes = [
    {
      path: 'lang/:id/:route',
      component: LangComponent
    },
    {
      path: 'lang/:id/:route/:subroute/:sub',
      component: LangComponent,
      pathMatch: 'prefix'
    },
    {
        path: 'lang/:id/:route/:subroute',
        component: LangComponent,
        pathMatch: 'prefix'
    },
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {
        path: 'error/498',
        redirectTo: 'auth/loginAs'
    },
    {
        path: 'error/401.1',
        redirectTo: 'auth/loginAs'
    },
    {
        path: 'error/0',
        redirectTo: 'error/408.1'
    },
    {
        path: '',
        component: FullLayoutComponent,
        canActivate: [
            AuthGuard
        ],
        data: {
            title: 'Home'
        },
        children: [
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'registrations',
                loadChildren: './registrations/registrations.module#RegistrationsModule'
            },
            {
                path: 'grades',
                loadChildren: './grades/grades.module#GradesModule'
            },
            {
                path: 'profile',
                loadChildren: './profile/profile.module#ProfileModule'
            },
            {
              path: 'requests',
              loadChildren: './requests/requests.module#RequestsModule'
            },
            {
                path: 'messages',
                loadChildren: './messages/messages.module#MessagesModule'
            },
            {
                path: 'info',
                loadChildren: './info-pages/info-pages.module#InfoPagesModule'
            },
            {
                path: 'graduation',
                loadChildren: './graduation-importer.module#GraduationImporterModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
